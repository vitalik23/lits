package utils;

import java.util.Random;

public class ArrayUtil {

    public int[] generateRandomIntArray(int length, int range) {
        int[] array = new int[length];
        Random random = new Random();
        for (int i = 0; i < array.length; i++)
            array[i] = random.nextInt(range);
        return array;
    }
}
