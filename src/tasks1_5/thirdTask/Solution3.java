package tasks1_5.thirdTask;

//3. Найменший елемент масиву замінити цілою частиною середнього арифметичного всіх елементів.
// Якщо в масиві є декілька найменших елементів, то замінити останній з них.

import utils.ArrayUtil;

public class Solution3 {

    public static void main(String[] args) {

        int array[] = new ArrayUtil().generateRandomIntArray(10, 10);

        array[findLastMinElementOfArray(array)[1]] = averageArithmeticOfArrayElements(array);

    }

    private static int[] findLastMinElementOfArray(int array[]) {
        int minElement[] = new int[2];
        minElement[0] = array[0];

        for (int i = 0; i < array.length; i++) {
            if (array[i] <= minElement[0]) {
                minElement[0] = array[i];
                minElement[1] = i;
            }
        }
        return minElement;
    }

    private static int averageArithmeticOfArrayElements(int array[]) {
        int sum = 0;
        for (int element : array)
            sum += element;
        return sum / array.length;
    }
}
