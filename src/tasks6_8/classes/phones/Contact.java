package tasks6_8.classes.phones;

public class Contact {
    static int totalContacts;
    private int zipCode;
    private String number;
    private String firstName;
    private String lastName;
    private String eMail;
    private String address;

    public Contact(String number, String firstName, String lastName) {
        this.number = number;
        this.firstName = firstName;
        this.lastName = lastName;
        totalContacts++;
    }

    public Contact(String number, int zipCode, String firstName, String lastName, String eMail, String address) {
        this(number, firstName, lastName);
        this.zipCode = zipCode;
        this.eMail = eMail;
        this.address = address;
        totalContacts++;
    }

    public static int howMutchContacts() {
        return totalContacts;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
