package tasks6_8.classes.phones;

import tasks6_8.interfaces.IPhone;

import javax.swing.text.html.HTML;

public class Smartphone implements IPhone {

    @Override
    public void call(Contact contact) {
        System.out.println(
                String.format(
                        "Calling... to %s %s", contact.getFirstName(), contact.getLastName()
                ));
    }

    @Override
    public void sentSMS(Contact contact, String text) {
        System.out.println(
                String.format(
                        "SMS sent to %s %s with text: %s",
                        contact.getFirstName(), contact.getLastName(), text
                ));
    }

    public HTML openWebPage(String webAddress) {
        return new HTML();
    }

    public void sentEmail(String emailAddress, String text) {
        System.out.println(String.format("Email sent to %s with text:\n %s", emailAddress, text));
    }
}
