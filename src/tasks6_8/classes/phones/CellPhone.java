package tasks6_8.classes.phones;

import tasks6_8.interfaces.IPhone;

public class CellPhone extends Phone implements IPhone {

    @Override
    public void call(Contact contact) {
        System.out.println(
                String.format(
                        "Calling... to %s %s", contact.getFirstName(), contact.getLastName()
                ));
    }

    @Override
    public void sentSMS(Contact contact, String text) {
        System.out.println(
                String.format(
                        "SMS sent to %s %s", contact.getFirstName(), contact.getLastName()
                ));
    }

    public void ringCell() {
        super.ring();
        System.out.println("The cell phone is ringing!!!!!");
    }
}
