package tasks6_8.classes;


import java.util.Calendar;

public class Car {

    private int year;
    private String color;
    private String model;
    private Human owner;

    public Car() {
        this.year = 1986;
        this.color = "white";
        this.model = "Opel kadett";
        this.owner = new Human("Car Collection Inc");
    }

    public Car(String model, int year) {
        this.year = year;
        this.color = "white";
        this.model = model;
        this.owner = new Human("Car Collection Inc");
    }

    public Car(String model, String color, Human owner) {
        this.model = model;
        this.color = color;
        this.owner = owner;
        this.year = Calendar.getInstance().get(Calendar.YEAR);
    }

    @Override
    public String toString() {
        return String.format("Model:\t%s`%d.\nColor:\t%s.\nOwner:\t%s.",
                this.model, this.year, this.color, this.owner.getName());
    }
}
