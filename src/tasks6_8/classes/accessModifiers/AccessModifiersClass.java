package tasks6_8.classes.accessModifiers;

public class AccessModifiersClass {

    private String privateField;
    String packageField;
    protected String protectedField;
    public String publicField;

    public String getPrivateField() {
        return privateField;
    }

    public void setPrivateField(String number) {
        this.privateField = number;
    }
}
